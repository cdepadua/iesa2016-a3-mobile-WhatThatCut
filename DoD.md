# Definition of Done 

### Le Definition of Done est un sous ensemble des activités nécessaires pour créer un incrément de produit potentiellement livrable. 

Le Definition of Done doit être partagé par tous les membres l'équipe. 

Ainsi, si le Definition of Done n'est pas validé dans son intégralité, le travail effectué ne peut pas être encore considéré comme achevé. Dans notre cas, le Definition of Done se compose des points suivants : 

* `Beta Test effectués et fonctionnels de la fonctionnalités sur un ordinateur`
* `Faire un "git push" de chaque fonctionnalité -Faire un "git pull" sur les autres ordinateurs` 
* `Tester la fonctionnalité sur les autres machines`
* `Reunion de fin de journée en équipe`
* `Valider le fonctionnement de chaque fonctionnalités`
* `Faire un git commit avec un tag validant chaque story`
* `Faire un git push -Validation du boss`