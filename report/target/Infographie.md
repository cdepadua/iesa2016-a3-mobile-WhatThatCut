# -------- Mobilité dans le monde et en France en 2015 --------

Définition : 
La mobilité est la propriété ou le caractère de ce qui peut se déplacer ou être déplacé dans l'espace et par extension ou métaphoriquement changer de fonction (métier), d'aspect et de forme. Dans le monde du numérique, on parle plus souvent de nomadisme numérique.
Le nomadisme numérique consiste à utiliser des technologies électroniques sans-fil pour utiliser des informations numériques ou communiquer par la téléphonie mobile ou par Internet quel que soit l'endroit où l'on se trouve.

Ici, la mobilité concerne des individus qui utilisent un smartphone et/ou une tablette et qui sont regroupés sous le terme de mobinaute. 
La définition du mobinaute peut varier selon les infographies (soit seulement smartphone, soit smartphone, tablette, ordinateur portable et objet connecté).

## ----------- Monde -----------

Chiffres clés : 
* 7 milliards d’habitants
* Il y à plus de forfaits mobiles que d'individus sur terre (8,6 milliards vs 7 milliards d'individus)
* La plus grande progression de vente se fera en Afrique, Moyen Orient et Asie.
* 42% de la population a un accès à internet
* Le temps passé en ligne est d’environ 4h25 par jour dont en moyenne 2h25 sur les réseaux sociaux

## ----------- France -----------

Chiffres clés :
Une population de plus en plus connecté : 
31 millions de mobinautes en France

Fréquence de connexion : 
* chaque jour : 82 %
* chaque semaine : 13 %
* chaque mois : 5 %

Combien de mobinautes :
* 50% de personnes possèdent un smartphone
* 33% de foyers possèdent une tablette

Répartition par âges : 
* 11-15: 6,5%
* 16-24: 25,9%
* 25-34: 23,1%
* 50-64: 24,9%
* 65+: 14,6%

Répartition par sexe :

Répartition des OS :
* android : 59,2%
* ios : 21%
* windows : 7,5%
* Blackberry : 4%
* reste : 8,3%

Répartition des devices :

Liens Monde :

http://wearesocial.com/fr/etudes/digital-social-mobile-les-chiffres-2015

http://www.blogdumoderateur.com/usage-internet-2016/


Liens France: 

http://www.blogdumoderateur.com/usage-internet-2016/

http://www.himediagroup.com/wp-content/uploads/2015/11/FR-Deloitte_Usages-Mobiles-2015_Nov2015-1.pdf

https://fr.wikipedia.org/wiki/Nomadisme_num%C3%A9rique

http://www.lsa-conso.fr/plus-de-25-millions-de-smartphones-et-tablettes-seront-vendus-en-france-en-2015-infographie,212847

https://www.thinkwithgoogle.com/intl/fr-fr/research/#?content_type=Infographie&media_channels=mobile&sort=date&page=3


http://www.appmobile.paris/wp-content/uploads/2015/05/Inforgraphie-Chiffre-Mobile-2015.jpg

http://www.le-webmarketeur.com/wp-content/uploads/2012/08/infographie-le-M-Commerce.png

https://www.thinkwithgoogle.com/intl/fr-fr/

https://www.consumerbarometer.com/en

http://www.toute-la-franchise.com/vie-de-la-franchise-A21921-en-2015-le-taux-d-equipement-en-sm.html


